# -*- coding: utf-8 -*-

import configparser
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, InlineQueryHandler
from telegram import InlineQueryResultArticle, ChatAction, InputTextMessageContent
from uuid import uuid4
import subprocess
import time
import logging
import sqlite3 as lite
import datetime
import urllib



logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))


config = configparser.ConfigParser()
config.read('bot.ini')

updater = Updater(token=config['KEYS']['bot_api'])
dispatcher = updater.dispatcher


def get_status():
    global db_file
    con = lite.connect(db_file)
    cur = con.cursor()
    cur.execute('SELECT * FROM Wgs')
    data = cur.fetchall()
    total = len(data)
    cur.execute('SELECT * FROM Wgs WHERE inPerimeter = 1')
    data = cur.fetchall()
    in_peri = len(data)
    cur.execute('SELECT * FROM Wgs WHERE inPerimeter = 1 AND Done = 0')
    data = cur.fetchall()
    new_ads = len(data)
    cur.close()
    return (total, in_peri, new_ads)


def start(bot, update):
    if update.message.from_user.id != int(config['ADMIN']['id']):
        bot.sendMessage(chat_id=update.message.chat_id,
                        text="It seems like you aren't allowed to use me. :(")
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text="hello, i am the wg bot")
        

def send_ads(bot, update, n_of_ads):
    global db_file
    con = lite.connect(db_file)
    cur = con.cursor()
    cur.execute('SELECT * FROM Wgs WHERE inPerimeter = 1 AND Done = 0 ORDER BY Created DESC LIMIT ' + str(n_of_ads))
    data = cur.fetchall()

    for row in data:
 
        msg = ''
        msg += '<b>Erstellt am:</b> ' +    datetime.datetime.fromtimestamp((row[3])).strftime('%d.%m.%Y') + '\n'
        msg += '<b>Miete:</b> ' + row[6] + '\n'
        msg += '<b>Ab:</b> ' + row[4] + ' '
        msg += '<b>bis:</b> ' + row[5] + '\n'
        msg += '<b>Adresse:</b> ' + row[7] + '\n'
        msg += '<b>Distanz zum HB:</b> ' + str(round(float(row[12]), 2)) + ' km\n'
        msg += '<b>Google maps:</b> ' + row[2].replace(' ', '+') + '\n' #<a href="' + row[2] + '">klick </a>\n'
        msg += '<b>Beschreibung:</b> ' + row[8] + '\n'
        msg += '<b>Wir suchen:</b> ' + row[10] + '\n'
        msg += '<b>Wir sind:</b> ' + row[11] + '\n'
        msg += '<b>Link:</b> <a href="https://www.wgzimmer.ch' + row[1] + '">klick</a>\n'

        try:
            bot.sendMessage(chat_id=update.message.chat_id,text=msg, parse_mode="HTML")
            # send imges
            imgs = row[9].split(',')
            for image_now in imgs:
                if image_now:
                    bot.sendPhoto(chat_id=154786379, photo=image_now)
        except:
            print 'fail'

        # mark as done
        cur.execute('UPDATE Wgs SET Done = ? WHERE Id = ?', (1, row[0]))

    con.commit()
    # con.close()



def execute(bot, update):
    try:
        user_id = update.message.from_user.id
        command = update.message.text
        inline = False
    except AttributeError:
        # Using inline
        user_id = update.inline_query.from_user.id
        command = update.inline_query.query
        inline = True


    if user_id == int(config['ADMIN']['id']):
        if command == 'status':
            (total, in_peri, new_ads) = get_status()
            output = 'status: \ntotal entries: '  + str(total) + '\nin perimeter: ' + str(in_peri) + '\nnew ads in perimeter: ' + str(new_ads)
            bot.sendMessage(chat_id=update.message.chat_id, text=output, parse_mode="HTML")
        elif command == 'get_1':
            send_ads(bot, update, 1)
        elif command == 'get_5':
            send_ads(bot, update, 5)
        elif command == 'get_20':
            send_ads(bot, update, 20)
        elif command == 'get_100':
            send_ads(bot, update, 1)
        elif command == '/bla':
            bot.sendMessage(chat_id=update.message.chat_id, text='bla', parse_mode="HTML")
        else:
            output = 'commands: \nstatus: displays status of system\nget_1: displays the most recent unseen ad\nget_5: displays five most recent unseen ads\nget_all: displays all unseen ads'
            bot.sendMessage(chat_id=update.message.chat_id,text=output, parse_mode="HTML")


     



db_file = 'wgs.db'

start_handler = CommandHandler('start', start)
execute_handler = MessageHandler([Filters.text], execute)

dispatcher.add_handler(start_handler)
dispatcher.add_handler(execute_handler)


dispatcher.add_error_handler(error)

updater.start_polling()
updater.idle()