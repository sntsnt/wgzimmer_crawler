

from bs4 import BeautifulSoup
import urllib2
import sqlite3 as lite
import sys
from geopy.geocoders import Nominatim
from math import radians, cos, sin, asin, sqrt
import time
import datetime
from matplotlib.path import Path



db_file = 'wgs.db'

con = lite.connect(db_file)
cur = con.cursor()

# cur.execute("CREATE TABLE Wgs(Id TEXT, Link TEXT, GeoLink TEXT, Created INTEGER, Ab TEXT, Bis TEXT, Miete TEXT, Adresse TEXT, Beschreibung TEXT, Images TEXT, WirSuchen TEXT, WirSind TEXT, Distance REAL, InPerimeter BOOLEAN, Done BOOLEAN)")

# ? https://map.geo.admin.ch/?topic=ech&lang=en&bgLayer=ch.swisstopo.pixelkarte-farbe&layers=ch.swisstopo.zeitreihen,ch.bfs.gebaeude_wohnungs_register,ch.bav.haltestellen-oev,ch.swisstopo.swisstlm3d-wanderwege,KML%7C%7Chttps:%2F%2Fpublic.geo.admin.ch%2F9sEaGCPDSnq2BhamFS0y7w&layers_visibility=false,false,false,false,true&layers_timestamp=18641231,,,,&E=2683720.79&N=1247837.14&zoom=7
perimeter = [[8.541532888303735,47.37851508087368], [8.525439723745214,47.383232618732556], [8.525033106081887,47.386024920927774], 
			 [8.526492894434517,47.38778771474205], [8.534649004024983,47.391511060015915], [8.539084638179066,47.39142392585778], 
			 [8.547761534074194,47.38668599148308], [8.548285066029667,47.380024567955054], [8.548558488051828,47.37725593450661], 
			 [8.544869012807665,47.376549226277916], [8.541435052622553,47.3769868419648]]


def id_in_db(id, cur):
	cur.execute("SELECT * FROM Wgs WHERE Id = '"+id+"'")
	row = cur.fetchone()
	return not (row == None)

def get_pos_from_addr(addr):
	try:
		geolocator = Nominatim()
		location = geolocator.geocode(addr)
		c = [location.longitude, location.latitude]
		return c
	except:
		return -1

def get_dist_from_hb(location):
	dist = 0
	try:
		dist = haversine(location[0], location[1], 8.540222, 47.377953)
	except:
		dist = 0

	return dist

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    km = 6367 * c
    return km


def point_in_polygon(point, poly):
    return Path(poly).contains_point(point)




url = 'https://www.wgzimmer.ch/wgzimmer/search/mate.html?'
# make a string with the request type in it:
method = "POST"
# create a handler. you can specify different handlers here (file uploads etc)
# but we go for the default
handler = urllib2.HTTPHandler()
# create an openerdirector instance
opener = urllib2.build_opener(handler)
# build a request
form_data = 'query=&priceMin=50&priceMax=1400&state=zurich-stadt&permanent=true&student=none&country=ch&orderBy=MetaData%2F%40mgnl%3Alastmodified&orderDir=descending&startSearchMate=true&wgStartSearch=true'
request = urllib2.Request(url, data=form_data)
# add any other information you want
request.add_header('Origin', 'https://www.wgzimmer.ch')
request.add_header('Referer', 'https://www.wgzimmer.ch/wgzimmer/search/mate.html')
request.add_header('User-Agent', 'Mozilla/5.0')

# overload the get method function with a small anonymous function...
request.get_method = lambda: method
# try it; don't forget to catch the result
try:
    connection = opener.open(request)
except urllib2.HTTPError,e:
    connection = e

# check. Substitute with appropriate HTTP code.
if connection.code == 200:
    data = connection.read()
else:
	print 'failed to grab search results'


# start parsing shit
soup = BeautifulSoup(data)

ad = {}

lis = soup.find_all('li')

for li in lis:
	is_valid_li = li.find_all('span', class_='create-date')
	if is_valid_li:
		ad['created'] = time.mktime(datetime.datetime.strptime(is_valid_li[0].get_text().strip(), "%d.%m.%Y").timetuple())
		ad['id'] = li.a['id']
		ad['link'] = li.a.next_sibling.next_sibling['href']

		if not id_in_db(ad['id'], cur):
			req = urllib2.Request('https://www.wgzimmer.ch'+ad['link'])
			req.add_header('Referer', 'https://www.wgzimmer.ch'+ad['link'])
			req.add_header('User-Agent', 'Mozilla/5.0')
			html_ad = urllib2.urlopen(req)

			soup_ad = BeautifulSoup(html_ad)

			date_cost = soup_ad.find_all("div", class_="wrap col-wrap date-cost")  #<div class="wrap col-wrap date-cost">
			temp = date_cost[0].find_all("p")
			ad['ab'] =  temp[0].get_text().replace('Ab dem ','') 
			ad['bis'] = temp[1].get_text().replace('Bis ','') 
			ad['miete'] = temp[2].get_text().replace('Miete / Monat sFr. ','') 
			
			address_region = soup_ad.find_all("div", class_="wrap col-wrap adress-region")  
			temp = address_region[0].find_all("p")
			ad['adresse'] =  temp[1].get_text().replace('Adresse ', '') + ', ' + temp[2].get_text().replace('Ort ', '')

			coord_flat = get_pos_from_addr(ad['adresse'])
			ad['distance'] = 0
			ad['inPerimeter'] = False
			print coord_flat
			if coord_flat and coord_flat != -1:
				print coord_flat[0], coord_flat[1]
				ad['distance'] = get_dist_from_hb(coord_flat)
				ad['inPerimeter'] = point_in_polygon(coord_flat, perimeter)

			print('flat withing perimeter: %s' % ad['inPerimeter'])
			ad['geolink'] = address_region[0].find_all("a")[0]['href']
			
			desc_div = soup_ad.find_all("div", class_="wrap col-wrap mate-content")  
			ad['beschreibung'] = desc_div[0].find_all("p")[0].get_text().replace('Das Zimmer ist', 'Das Zimmer ist ')

			images_div =  soup_ad.find_all("div", class_="image")
			images_str = ''
			if images_div:
				temp = images_div[0].find_all("a")
				for img in temp:
					images_str += 'https://www.wgzimmer.ch' + img['href'] + ','

			ad['images'] = images_str

			suchen_div = soup_ad.find_all("div", class_="wrap col-wrap room-content")  
			ad['wirsuchen'] = suchen_div[0].find_all("p")[0].get_text()

			person_div = soup_ad.find_all("div", class_="wrap col-wrap person-content")  
			ad['wirsind'] = person_div[0].find_all("p")[0].get_text()


			cur.execute('insert into Wgs values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
				(ad['id'], ad['link'], ad['geolink'], int(ad['created']), ad['ab'], ad['bis'], ad['miete'], ad['adresse'], 
					ad['beschreibung'], ad['images'], ad['wirsuchen'], ad['wirsind'], str(ad['distance']), ad['inPerimeter'], 0))

			con.commit()

			print 'wg ' + ad['link'] 

