from telegram.ext import Updater, CommandHandler, Job
import logging
import sqlite3 as lite
import datetime
import urllib
import configparser

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

db_file = 'wgs.db'

config = configparser.ConfigParser()
config.read('bot.ini')



def send_ads(bot, n_of_ads):
    global db_file
    con = lite.connect(db_file)
    cur = con.cursor()
    cur.execute('SELECT * FROM Wgs WHERE inPerimeter = 1' + ' AND Done = 0 ORDER BY Created DESC LIMIT ' + str(n_of_ads))
    data = cur.fetchall()

    for row in data:
 
        msg = ''
        msg += '<b>Erstellt am:</b> ' +    datetime.datetime.fromtimestamp((row[3])).strftime('%d.%m.%Y') + '\n'
        msg += '<b>Miete:</b> ' + row[6] + '\n'
        msg += '<b>Ab:</b> ' + row[4] + ' '
        msg += '<b>bis:</b> ' + row[5] + '\n'
        msg += '<b>Adresse:</b> ' + row[7] + '\n'
        msg += '<b>Distanz zum HB:</b> ' + str(round(float(row[12]), 2)) + ' km\n'
        msg += '<b>Google maps:</b> ' + row[2].replace(' ', '+') + '\n' #<a href="' + row[2] + '">klick </a>\n'
        msg += '<b>Beschreibung:</b> ' + row[8] + '\n'
        msg += '<b>Wir suchen:</b> ' + row[10] + '\n'
        msg += '<b>Wir sind:</b> ' + row[11] + '\n'
        msg += '<b>Link:</b> <a href="https://www.wgzimmer.ch' + row[1] + '">klick</a>\n'

        try:
            bot.sendMessage(chat_id=config['ADMIN']['id'],text=msg, parse_mode="HTML")
            # send imges
            imgs = row[9].split(',')
            for image_now in imgs:
                if image_now:
                    bot.sendPhoto(chat_id=config['ADMIN']['id'], photo=image_now)
        except:
            print 'fail'

        # mark as done
        cur.execute('UPDATE Wgs SET Done = ? WHERE Id = ?', (1, row[0]))

    con.commit()
    # con.close()


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    update.message.reply_text('Hi! Use /set <seconds> to set the update frequency in seconds of the notification bot. The bot will be started immediately. Use /unset to stop the notification bot.')



def alarm(bot, job):
    send_ads(bot, config['FILTER']['ads_per_flush'])


def set(bot, update, args, job_queue, chat_data):
    """Adds a job to the queue"""
    chat_id = update.message.chat_id
    try:
        # args[0] should contain the time for the timer in seconds
        due = int(args[0])
        if due < 0:
            update.message.reply_text('Sorry we can not go back to future!')
            return

        # Add job to queue
        job = job_queue.run_once(alarm, due, context=chat_id)
        chat_data['job'] = job

        # job = Job(alarm, due, repeat=True, context=chat_id)
        # chat_data['job'] = job
        # job_queue.put(job)

        update.message.reply_text('Notification-bot started!')
        send_ads(bot, config['FILTER']['ads_per_flush'])

    except (IndexError, ValueError):
        update.message.reply_text('Usage: /set <seconds>')


def unset(bot, update, chat_data):
    """Removes the job if the user changed their mind"""

    if 'job' not in chat_data:
        update.message.reply_text('No bot was running.')
        return

    job = chat_data['job']
    job.schedule_removal()
    del chat_data['job']

    update.message.reply_text('Bot stopped.')


def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"' % (update, error))


def main():
    updater = Updater(config['KEYS']['bot_api'])

    j = updater.job_queue


    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", start))
    dp.add_handler(CommandHandler("set", set,
                                  pass_args=True,
                                  pass_job_queue=True,
                                  pass_chat_data=True))
    dp.add_handler(CommandHandler("unset", unset, pass_chat_data=True))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Block until you press Ctrl-C or the process receives SIGINT, SIGTERM or
    # SIGABRT. This should be used most of the time, since start_polling() is
    # non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()